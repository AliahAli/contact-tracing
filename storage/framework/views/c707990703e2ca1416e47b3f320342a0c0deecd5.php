

<?php $__env->startSection('title', 'Edit User'); ?>

<?php $__env->startSection('vendor-style'); ?>
<!-- vendor css files -->
<link rel="stylesheet" href="<?php echo e(asset(mix('vendors/css/forms/wizard/bs-stepper.min.css'))); ?>">
<link rel="stylesheet" href="<?php echo e(asset(mix('vendors/css/forms/select/select2.min.css'))); ?>">
<link rel="stylesheet" href="<?php echo e(asset(mix('vendors/css/pickers/pickadate/pickadate.css'))); ?>">
  <link rel="stylesheet" href="<?php echo e(asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css'))); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-style'); ?>
<!-- Page css files -->
<link rel="stylesheet" href="<?php echo e(asset(mix('css/base/plugins/forms/form-validation.css'))); ?>">
<link rel="stylesheet" href="<?php echo e(asset(mix('css/base/plugins/forms/form-wizard.css'))); ?>">
<link rel="stylesheet" href="<?php echo e(asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css'))); ?>">
<link rel="stylesheet" href="<?php echo e(asset(mix('css/base/plugins/forms/pickers/form-pickadate.css'))); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Edit <span><?php echo e($device->name); ?></span></h4> 
        </div>
    </div>
    <div class="card">
        <div class="card-body px-2 py-2 align-items-center">
            <form action="<?php echo e(route('devices.update', $device->id)); ?>" method="POST">
                <?php echo csrf_field(); ?>
                <?php echo method_field('POST'); ?>

                <div class="mb-1 row">
                    <label class="col-sm-2 col-form-label" for="first-name">Name :</label>
                    <div class="col-sm-6">
                        <input  type="text" class="form-control" id="name" name="name"  value="<?php echo e($device->name); ?>"/>
                    </div>
                </div>
                <div class="mb-1 row">
                    <label class="col-sm-2 col-form-label" for="first-name" >Mac Address :</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="mac_address" name="mac_address"  value="<?php echo e($device->mac_address); ?>" />
                    </div>
                </div>
                <div class="mb-1 row">
                    <label class="col-sm-2 col-form-label" for="first-name">IP Address :</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="ip_address" name="ip_address" value="<?php echo e($device->ip_address); ?>"/>
                    </div>
                </div>

                <div class="alert-body d-flex align-items-center justify-content-between flex-wrap p-2">
                    <div class="me-1">
                      <a class="btn btn-outline-secondary" href="<?php echo e(route('devices-list')); ?>">
                        <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                        <span>Back</span>
                      </a>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Submit</a>
                  </div>
            </form>
        </div>
    </div>     
</section>

<!-- /Horizontal Wizard -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('vendor-script'); ?>
<!-- vendor files -->
<script src="<?php echo e(asset(mix('vendors/js/forms/wizard/bs-stepper.min.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/forms/select/select2.full.min.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/forms/validation/jquery.validate.min.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/pickers/pickadate/picker.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/pickers/pickadate/picker.date.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/pickers/pickadate/picker.time.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/pickers/pickadate/legacy.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js'))); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-script'); ?>
<!-- Page js files -->
<script src="<?php echo e(asset(mix('js/scripts/forms/form-wizard.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('js/scripts/forms/form-repeater.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('js/scripts/forms/pickers/form-pickers.js'))); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/contentLayoutMaster', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Aliah\Desktop\Projects\contact-tracing\resources\views/devices/devices-edit.blade.php ENDPATH**/ ?>