

<?php $__env->startSection('title', 'User List'); ?>

<?php $__env->startSection('content'); ?>
<!-- Basic Tables start -->
<div class="row" id="basic-table">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">List of Users</h4>
                <div class="row">
                    <div class="col-md-6">
                        <form class="flex flex-col md:flex-row flex-no wrap items-left">
                            <input :value="$page.props.device_search" type="text" name="device_search" class="form-control" placeholder="Search...">
                        </form>
                    </div>
                    <div class="col-md-6">
                      <a class="btn btn-primary" href="<?php echo e(route('devices-create')); ?>">
                        <i data-feather="user-plus" class="align-middle me-sm-25 me-0"></i>
                        <span>Add User</span>
                      </a>
                    </div>
                </div>

            </div>

        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Mac Address</th>
                        <th>IP Address</th>
                        <th class="cell-fit">Actions</th>
                    </tr>
                </thead>
                <?php $__currentLoopData = $devices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $device): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($device->name); ?></td>
                    <td><?php echo e($device->mac_address); ?></td>
                    <td><?php echo e($device->ip_address); ?></td>
                    <td class="text-center">
                        <div class="d-inline-flex">
                            <a class="me-1" href="<?php echo e(route('devices-edit',['device' => $device])); ?>" data-bs-toggle="tooltip" data-bs-placement="top" title="Edit User">
                                <i data-feather="edit" class="font-medium-3"></i>
                            </a>
                            <a class="me-1" href="<?php echo e(route('devices-nearby', ['device' => $device])); ?>" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Show Nearby Devices">
                                <i data-feather="external-link" class="font-medium-3"></i>
                            </a>
                        </div>
                    </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <?php if(count($devices) == 0): ?>
                <tr>
                    <td class="border-t px-2 py-2 text-center" colspan="6">No device found.</td>
                </tr>
                <?php endif; ?>

            </table>
        </div>
    </div>
</div>
<!-- Basic Tables end -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/contentLayoutMaster', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Aliah\Desktop\Projects\contact-tracing\resources\views/devices/devices-list.blade.php ENDPATH**/ ?>