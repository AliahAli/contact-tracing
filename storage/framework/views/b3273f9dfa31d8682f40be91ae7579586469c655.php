

<?php $__env->startSection('title', 'Dashboard'); ?>

<?php $__env->startSection('vendor-style'); ?>
<!-- vendor css files -->
<link rel="stylesheet" href="<?php echo e(asset(mix('vendors/css/charts/apexcharts.css'))); ?>">
<link rel="stylesheet" href="<?php echo e(asset(mix('vendors/css/extensions/toastr.min.css'))); ?>">
<link rel="stylesheet" href="<?php echo e(asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css'))); ?>">
<link rel="stylesheet" href="<?php echo e(asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css'))); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-style'); ?>
<!-- Page css files -->
<link rel="stylesheet" href="<?php echo e(asset(mix('css/base/plugins/charts/chart-apex.css'))); ?>">
<link rel="stylesheet" href="<?php echo e(asset(mix('css/base/plugins/extensions/ext-component-toastr.css'))); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Dashboard Analytics Start -->
<section id="dashboard-analytics">
  <div class="row match-height">
    <!-- Total Devices Card starts -->
    <div class="col-lg-3 col-sm-6 col-12">
      <div class="card">
        <div class="card-header flex-column align-items-start pb-0">
          <div class="avatar bg-light-primary p-50 m-0">
            <div class="avatar-content m-1">
              <i data-feather="users" class="font-large-5"></i>
            </div>
          </div>
          <h2 class="fw-bolder mt-1 font-large-3"><?php echo e(count($devices)); ?></h2>
          <p class="card-text font-medium-2">Devices Registered</p>
        </div>
        <div id="devices-chart"></div>
      </div>
    </div>
    <!-- Total Devices Card ends -->

    <!-- Avg Sessions Chart Card starts -->
    <div class="col-lg-9 col-12">
      <div class="card p-2">
        <div class="card-body">
          <div class="row pb-50">
            <div class="col-12 revenue-report-wrapper">
              <div class="d-sm-flex justify-content-between align-items-center mb-3">
                <h2 class="fw-bolder mb-25">Total Devices Contacted by Hours</h2>
                <div class="d-flex align-items-center">
                  <div class="d-flex align-items-center me-2">
                    <span class="bullet bullet-primary font-small-3 me-50 cursor-pointer"></span>
                    <span>Devices Contacted</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row avg-sessions pt-50">
            <div id="avg-nearby-devices-chart"></div>
          </div>
          <hr />
          <div class="row avg-sessions pt-50">
            <div class="col-12 mb-2">
              <p class="mb-50">The bar chart shows total of devices contacted for each hour in a day.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Avg Sessions Chart Card ends -->
  </div>
</section>
<!-- Dashboard Analytics end -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('vendor-script'); ?>
<!-- vendor files -->
<script src="<?php echo e(asset(mix('vendors/js/charts/apexcharts.min.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/extensions/toastr.min.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/extensions/moment.min.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js'))); ?>"></script>
<script>

  $(document).ready(function() {

    var $textMutedColor = '#b9b9c3';
    var $avgSessionStrokeColor2 = '#ebf0f7';
    var $avgNearbyDevicesChart = document.querySelector('#avg-nearby-devices-chart');
    var avgNearbyDevicesChartOptions;
    var avgNearbyDevicesChart;
    var devicesChartOptions;
    var devicesChart;
    var $devicesChart = document.querySelector('#devices-chart');

    // Average Nearby Devices Chart
    // ----------------------------------
    avgNearbyDevicesChartOptions = {
      chart: {
        type: 'bar',
        height: 240,
        stacked: true,
        toolbar: { show: false },
      },
      plotOptions: {
        bar: {
          columnWidth: '50%',
          endingShape: 'rounded'
        },
        distributed: true
      },
      colors: [
        window.colors.solid.primary, 
        window.colors.solid.warning
      ],
      series: [
        {
          name: 'Per Hours',
          data: [
            <?php
            for ($i = 0; $i <= 23; $i++){
              $bool = false;
              foreach ($nearby_devices as $key => $value){
                if($i == $key){
                  echo count($nearby_devices[$key]) . ', ';
                  $bool = true;
                }
              }
              if($bool == false){
                echo '0, ';
              }
            }
            ?>
        ]
        }
    ],
      dataLabels: {
        enabled: false
      },
      legend: {
        show: false
      },
      grid: {
        padding: {
          top: -20,
          bottom: -10
        },
        yaxis: {
          lines: { show: false }
        }
      },
      xaxis: {
        categories: [
          <?php
            for ($i = 0; $i <= 23; $i++){
              echo $i . ', ';
            }
          ?>
        ],
        labels: {
          style: {
            colors: $textMutedColor,
            fontSize: '0.86rem'
          },
        },
        axisTicks: {
          show: false
        },
        axisBorder: {
          show: false
        }
      },
      yaxis: {
        labels: {
          style: {
            colors: $textMutedColor,
            fontSize: '0.86rem'
          },
        }
      },
    };
    avgNearbyDevicesChart = new ApexCharts($avgNearbyDevicesChart, avgNearbyDevicesChartOptions);
    avgNearbyDevicesChart.render();

    // Devices Registered Chart
    // ----------------------------------

    devicesChartOptions = {
      chart: {
        height: 240,
        type: 'area',
        toolbar: {
          show: false
        },
        sparkline: {
          enabled: true
        },
        grid: {
          show: false,
          padding: {
            left: 0,
            right: 0
          }
        }
      },
      colors: [window.colors.solid.primary],
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth',
        width: 2.5
      },
      fill: {
        type: 'gradient',
        gradient: {
          shadeIntensity: 0.9,
          opacityFrom: 0.7,
          opacityTo: 0.5,
          stops: [0, 80, 100]
        }
      },
      series: [
        {
          name: 'Subscribers',
          data: [28, 40, 36, 52, 38, 60, 55]
        }
      ],
      xaxis: {
        labels: {
          show: false
        },
        axisBorder: {
          show: false
        }
      },
      yaxis: [
        {
          y: 0,
          offsetX: 0,
          offsetY: 0,
          padding: { left: 0, right: 0 }
        }
      ],
      tooltip: {
        x: { show: false }
      }
    };
    devicesChart = new ApexCharts($devicesChart, devicesChartOptions);
    devicesChart.render();
  });
</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-script'); ?>
<!-- Page js files -->
<script src="<?php echo e(asset(mix('js/scripts/pages/dashboard-analytics.js'))); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/contentLayoutMaster', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Aliah\Desktop\Projects\contact-tracing\resources\views//content/dashboard/dashboard-contact-tracing.blade.php ENDPATH**/ ?>