<!-- BEGIN: Footer-->
<footer class="footer footer-light <?php echo e(($configData['footerType'] === 'footer-hidden') ? 'd-none':''); ?> <?php echo e($configData['footerType']); ?>">
  <p class="clearfix mb-0">
    <span class="float-md-start d-block d-md-inline-block mt-25">Powered by
      <script>document.write(new Date().getFullYear())</script><a class="ms-25" href="https://webhaus.my" target="_blank">WEBHAUS</a>,
      <span class="d-none d-sm-inline-block">All rights Reserved</span>
    </span>
    <span class="float-md-end d-none d-md-block">Hand-crafted & Made with<i data-feather="heart"></i></span>
  </p>
</footer>
<button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
<!-- END: Footer-->
<?php /**PATH C:\Users\Aliah\Desktop\Projects\contact-tracing\resources\views/panels/footer.blade.php ENDPATH**/ ?>