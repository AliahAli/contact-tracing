

<?php $__env->startSection('title', 'Nearby Device'); ?>

<?php $__env->startSection('vendor-style'); ?>

<link rel="stylesheet" href="<?php echo e(asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css'))); ?>">
<link rel="stylesheet" href="<?php echo e(asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css'))); ?>">
<link rel="stylesheet" href="<?php echo e(asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css'))); ?>">
<link rel="stylesheet" href="<?php echo e(asset(mix('vendors/css/pickers/pickadate/pickadate.css'))); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-style'); ?>

<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/base/plugins/forms/pickers/form-flat-pickr.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset(mix('css/base/plugins/forms/pickers/form-pickadate.css'))); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<!-- Advanced Search -->
<section id="basic-datatable flatpickr">
    <div class="row">
        <div class="card bg-light-primary">
            <ul class="list-unstyled px-2 py-2">
                <li>
                    <span class="fw-bolder">User Name:</span>
                    <span><?php echo e($device->name); ?></span>
                </li>
                <li class="mt-1">
                    <span class="fw-bolder">Mac Address:</span>
                    <span><?php echo e($device->mac_address); ?></span>
                </li>
                <li class="mt-1">
                    <span class="fw-bolder">Ip Address:</span>
                    <span><?php echo e($device->ip_address); ?></span>
                </li>
            </ul>
        </div>
        <div class="card">
            <div class="card-header border-bottom">
                <h4 class="card-title">List of Nearby Devices</h4>
            </div>

            <!--Search Form -->
            <div class="card-body mt-2">
                <form action="<?php echo e(route('devices-nearby',['device' => $device])); ?>" method="GET" role="search">
                    <?php echo e(csrf_field()); ?>

                    <div class="row">
                        <div class="col-md-5 mb-1">
                            <label class="form-label" for="fp-range">Date from</label>
                            <input type="text" id="search" name="from" class="form-control flatpickr-date-time" placeholder="YYYY-MM-DD HH:MM" value = "<?php echo e(request()->from); ?>"/>
                        </div>
                        <div class="col-md-5 mb-1">
                            <label class="form-label" for="fp-time">Date to</label>
                            <input type="text" name="to" id="search" class="form-control flatpickr-date-time" placeholder="YYYY-MM-DD HH:MM" value = "<?php echo e(request()->to); ?>"/>
                        </div>
                        <div class="col-md-2 alert-body d-flex align-items-center justify-content-between flex-wrap p-2 float-right">
                            <input class="btn btn-primary" id="search" name="search" type="submit" value="Search">
                        </div>
                    </div>
                </form>
            </div>
            <hr class="my-0" />
            <div class="card-datatable">
                <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>Contact Name</th>
                            <th>Mac Address</th>
                            <th>RSSI</th>
                            <th lass="cell-fit">Scanned At</th>
                        </tr>
                    </thead>
                    <?php $__currentLoopData = $device->nearbyDevices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($data->owner_name); ?></td>
                        <td><?php echo e($data->mac_address); ?></td>
                        <td><?php echo e($data->rssi); ?></td>
                        <td><?php echo e($data->scanned_at); ?></td>

                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php if($device->nearbyDevices->count() == 0): ?>
                    <tr>
                        <td class="border-t px-6 py-4 text-center" colspan="6">No nearby device found.</td>
                    </tr>
                    <?php endif; ?>
                </table>

                <div class="mt-2">
                    <?php echo e($device->nearbyDevices->appends(['from' => request()->from, 'to' => request()->to])->links()); ?>

                </div>
            </div>
        </div>
</section>
<!--/ Advanced Search -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('vendor-script'); ?>

<script src="<?php echo e(asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/pickers/pickadate/picker.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/pickers/pickadate/picker.date.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/pickers/pickadate/picker.time.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/pickers/pickadate/legacy.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/pagination/jquery.bootpag.min.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('vendors/js/pagination/jquery.twbsPagination.min.js'))); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-script'); ?>

<script src="<?php echo e(asset(mix('js/scripts/tables/table-datatables-advanced.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('js/scripts/forms/pickers/form-pickers.js'))); ?>"></script>
<script src="<?php echo e(asset(mix('js/scripts/pagination/components-pagination.js'))); ?>"></script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/contentLayoutMaster', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Aliah\Desktop\Projects\contact-tracing\resources\views/devices/devices-nearby.blade.php ENDPATH**/ ?>